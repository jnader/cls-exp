%% Display reference trajectory

close all

load trajs_and_per/trajs.mat
opt_cond = 'PI_opt'; % 'IG_InputSat' or 'IG_Target' or 'PI_opt'
traj_num = 1; %
%wp = waypointsmat; wp_t = waypoints_t;

wp = [];
wp_t = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp_t']);
for i = 1:length(wp_t)
    wp_tmp = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp']);
    wp = [wp squeeze(wp_tmp(i, :, :))];
end

t_traj = linspace(0, wp_t(end), floor(100*wp_t(end)));
dt = t_traj(2) - t_traj(1);
xyzyaw_t = zeros(length(t_traj), 4);

for i = 1:length(t_traj)
    xyzyaw_t(i, :) = waypoints2trajectory(wp_t, wp, t_traj(i));
end
dxyzyaw_t = diff(xyzyaw_t)/dt;
ddxyzyaw_t = diff(dxyzyaw_t)/dt;
dddxyzyaw_t = diff(ddxyzyaw_t)/dt;
ddddxyzyaw_t = diff(dddxyzyaw_t)/dt;

ddddcurve = ddddxyzyaw_t(:, 1:3);
curve = xyzyaw_t(1:size(ddddcurve), 1:3);
dcurve = dxyzyaw_t(1:size(ddddcurve), 1:3);
ddcurve = ddxyzyaw_t(1:size(ddddcurve), 1:3);
dddcurve = dddxyzyaw_t(1:size(ddddcurve), 1:3);

rcurve = zeros(1, size(ddddcurve, 1));
drcurve = zeros(1, size(ddddcurve, 1));
ddrcurve = zeros(1, size(ddddcurve, 1));

for i=1:length(rcurve)
    if norm(cross(ddcurve(i, :), dcurve(i, :))) ~= 0 && norm(cross(dddcurve(i, :), ddcurve(i, :))) ~= 0 && norm(cross(ddddcurve(i, :), dddcurve(i, :))) ~= 0
    	rcurve(i) = norm(dcurve(i, :))^3/norm(cross(ddcurve(i, :), dcurve(i, :)));
        drcurve(i) = norm(ddcurve(i, :))^3/norm(cross(dddcurve(i, :), ddcurve(i, :)));
        ddrcurve(i) = norm(ddcurve(i, :))^3/norm(cross(dddcurve(i, :), ddcurve(i, :)));
    else
        disp('Error in vectorial products !')
    end
end

rpmin = min(rcurve)
rvmin = min(drcurve)
ramin = min(drcurve)


% pos

figure(1);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelpos = {'x [m]', 'y [m]', 'z [m]', ' \psi [rad]'};

for i=1:4
    subplot(2, 2, i)
    plot(t_traj, xyzyaw_t(:, i), 'LineWidth', 2, 'Color', 'b')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelpos{i}, 'FontSize', 14)
    title('Position', 'FontSize', 16)
    grid on
    axis auto
end

% vel

figure(2);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelvel = {'v_x [m.s^{-1}]', 'v_y [m.s^{-1}]', 'v_z [m.s^{-1}]', 'v_{\psi} [rad.s^{-1}]'};

for i=1:4
    subplot(2, 2, i)
    plot(t_traj(1:end-1), dxyzyaw_t(:, i), 'LineWidth', 2, 'Color', 'k')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelvel{i}, 'FontSize', 14)
    title('Velocity', 'FontSize', 16)
    grid on
    axis auto
end


% acc

figure(3);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelacc = {'a_x [m.s^{-2}]', 'a_y [m.s^{-2}]', 'a_z [m.s^{-2}]', 'a_{\psi} [rad.s^{-2}]'};

for i=1:4
    subplot(2, 2, i)
    plot(t_traj(1: end-2), ddxyzyaw_t(:, i), 'LineWidth', 2, 'Color', 'r')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelacc{i}, 'FontSize', 14)
    
    grid on
    axis auto
end


figure(4)
set(gcf,'Units','centimeters','Position',[1 2 32 12],'Color', 'w');
for i = 1:3
    subplot(1, 3, i)
    if i == 1
        plot3(xyzyaw_t(:, 1), xyzyaw_t(:, 2), xyzyaw_t(:, 3), 'LineWidth', 2);
        xlabel(labelpos{1}, 'FontSize', 14)
        ylabel(labelpos{2}, 'FontSize', 14)
        zlabel(labelpos{3}, 'FontSize', 14)
        axis equal
    elseif i == 2
        plot3(dxyzyaw_t(:, 1), dxyzyaw_t(:, 2), dxyzyaw_t(:, 3), 'LineWidth', 2);
        xlabel(labelvel{1}, 'FontSize', 14)
        ylabel(labelvel{2}, 'FontSize', 14)
        zlabel(labelvel{3}, 'FontSize', 14)
        axis equal
    else
        plot3(ddxyzyaw_t(:, 1), ddxyzyaw_t(:, 2), ddxyzyaw_t(:, 3), 'LineWidth', 2);
        xlabel(labelacc{1}, 'FontSize', 14)
        ylabel(labelacc{2}, 'FontSize', 14)
        zlabel(labelacc{3}, 'FontSize', 14)
        axis equal
    end
end


