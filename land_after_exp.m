for i=1:length(robots)
    current_state = robots{i}.pom.frame('robot');
    current_pos = current_state.frame.pos;
    current_att = current_state.frame.att;
    eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);
    yaw = eul_angles(1);
    landpos = [current_pos.x, current_pos.y, 0.3, 0];
    robots{i}.maneuver.set_velocity_limit(0.25,0.25)
    robots{i}.nhfc.set_current_position();  % ('-a') % servo on current state with nhfc -a = asynchrone
    robots{i}.maneuver.set_current_state();  % start traj gen from this state
    nhfc_out = robots{i}.nhfc.servo('-a'); % start nhfc servoing
    maneuvergoto = robots{i}.maneuver.goto('-a', x0, y0, 0.3, yaw0, 0); % gen traj from ref + asked waypoint
end