% Get the current estimated position.
current_state = robots{1}.pom.frame('robot');

robots{1}.nhfc.set_current_position('-a');
robots{1}.maneuver.set_current_state('-a');
robots{1}.nhfc.servo('-a');
robots{1}.maneuver.goto('-a', current_state.frame.pos.x, current_state.frame.pos.y, current_state.frame.pos.z, yaw0, 0);
