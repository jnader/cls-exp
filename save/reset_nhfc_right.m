for i=1:length(robots)
    robots{i}.maneuver.set_velocity_limit(0.25,0.25)
    robots{i}.nhfc.set_current_position();  % ('-a') % servo on current state with nhfc -a = asynchrone
    robots{i}.maneuver.set_current_state();  % start traj gen from this state
    nhfc_out = robots{i}.nhfc.servo('-a'); % start nhfc servoing
    maneuvergoto = robots{i}.maneuver.goto('-a', x0r, y0, z0, yaw0, 0); % gen traj from ref + asked waypoint
end
disp('Go to right origin :')
disp(['x = ' num2str(x0r) ', y = ' num2str(y0), ', z = ' num2str(z0), ', yaw = ' num2str(yaw0)])
