% This script is an interactive menu used at the launch of `init.m` file to
% configure the experiment ahead. Mainly, it will ask the user a series of
% questions about the process of the experiment and will set some flags to
% avoid setting it manually everytime.

if flag_simulationReal==1
    disp('You are in simulation...')
    
    flag_ViconQualisys = 1;
    flag_mocap = 1;
    flag_vo = 0;
    flag_vs = 0;
    if clsexp == 1
        demo_choice = 7;
        quads_number = [3];
        disp(['7. Closed-loop sensitivity trials with Jetson ' num2str(quads_number) ','])
    else 
        demo_choice = 1;
        disp('You will use the joystick to move the drone...');
    end
    disp('*********');
else
    if clsexp==1
        disp('Qualisys will give the position/orientation of the drones.');
        flag_ViconQualisys = 2;
        flag_mocap = 1;
        flag_vo = 0;
        flag_vs = 0;
        demo_choice = 7;
        quads_number = [3];
        disp(['7. Closed-loop sensitivity experiments with Jetson ' num2str(quads_number) ','])
        input('Press enter to continue...');
        clc
    else
        disp('Please answer the following questions by entering the number corresponding to the desired entry...');
        disp('*********');
        input('Press enter to continue...');
        clc

        % Asking if we want to use Vicon/Qualisys and (or) Visual Odometry from
        % T265
        disp('Position/Orientation of drones is given by:');
        disp('1. Vicon');
        disp('2. Qualisys');
        disp('3. T265 Visual Odometry (with Qualisys for init)');
        disp('4. T265 Visual Odometry (with Vicon for init)');
        disp('5. Both (T265 + Qualisys)');
        disp('6. Both (T265 + Vicon)');
        pose_answer = input('Answer: ');
        % Setting flags based on the answer.
        switch pose_answer
            case 1
                flag_ViconQualisys = 1;
                flag_mocap = 1;
                flag_vo = 0;
            case 2
                flag_ViconQualisys = 2;
                flag_mocap = 1;
                flag_vo = 0;
            case 3
                flag_vo = 1;
                flag_mocap = 0;
                flag_ViconQualisys = 2; % not used
            case 4
                flag_vo = 1;
                flag_mocap = 0;
                flag_ViconQualisys = 1;
            case 5
                flag_mocap = 1;
                flag_vo = 1;
                flag_ViconQualisys = 2;
            case 6
                flag_mocap = 1;
                flag_vo = 1;
                flag_ViconQualisys = 1;
            otherwise % Default, Metivier
                error('You should specify one of the choices...!');
        end

        clc

        % Asking about the demo you want to launch.
        disp('Demo you want to launch');
        disp('1. Joystick drones');
        disp('2. Single vertical AprilTag visual servoing');
        disp('3. Multiple AprilTags visual servoing');
        disp('4. Drone DNN detection and visual servoing');
        demo_choice = input('Answer: ');
        % Setting flags based on the answer.
        switch demo_choice
            case 1 % using joystick to control all drones.
                flag_vs = 0;
            case 2 % visual servo on single vertical apriltag.
                quads_number = [2];
                flag_vs = 1;
            case 3 % visual servo on multiple apriltags.
                quads_number = [2];
                flag_vs = 1;
            case 4 % visual servo on drone detected.
                quads_number = [1 4];
                flag_vs = 0;
            otherwise % not yet implemented.
                flag_vs = 0;
        end
        clc
    end
end
