%%%%%%%%%%%%%%%%%%%%%%%%%%% SIMULINK UPDATER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -------------------------------------------------------------------------
% This file updates the Simulink model in order to match the new parameters
% defined by the initialization file init.m
% -------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if demo_choice ~= 2
    if clsexp==1
        modelname = 'cls_trajectory_20b';
        open(modelname)

        if flag_simulationReal == 1
            set_param([modelname '/genomixpubsim'],'commented','off'); % publish to vrep
            set_param([modelname '/state2pose_mk'],'commented','off');
            set_param([modelname '/pub_vrep_mk'],'commented','off');
            set_param([modelname '/genomixpubsim'],'Host',['localhost:1000' int2str(quads_number)]);
            set_param([modelname '/genomixpubsim'],'Instance',['mrsim_mk' int2str(quads_number)]);
            set_param([modelname '/mkQuadro/set_drone_state'],'Host',['localhost:1000' int2str(quads_number)]);
            set_param([modelname '/mkQuadro/set_drone_state'],'Instance',['nhfc_mk' int2str(quads_number)]);
            set_param([modelname '/get_drone_frame'],'Host',['localhost:1000' int2str(quads_number)]);
            set_param([modelname '/get_drone_frame'],'Instance',['pom_mk' int2str(quads_number)]);
        else
            set_param([modelname '/genomixpubsim'],'commented','on'); % no need to publish for vrep
            set_param([modelname '/state2pose_mk'],'commented','on');
            set_param([modelname '/pub_vrep_mk'],'commented','on');
            set_param([modelname '/mkQuadro/set_drone_state'],'Host',['jetson' int2str(quads_number) ':1000' int2str(quads_number)]);
            set_param([modelname '/mkQuadro/set_drone_state'],'Instance',['nhfc_mk' int2str(quads_number)]);
            set_param([modelname '/get_drone_frame'],'Host',['jetson' int2str(quads_number) ':1000' int2str(quads_number)]);
            set_param([modelname '/get_drone_frame'],'Instance',['pom_mk' int2str(quads_number)]);
        end
    else
        modelname = 'velocity_maneuver_2020b'; % simulink model to update
        open(modelname)

        quadrotormax = 6; % maximum amount of quadrotors in the model

        %% QUADROTORS ACTIVATION/DEACTIVATION

        % DEACTIVATE EVERY QUADROTOR

        for i = 1:quadrotormax
            set_param([modelname '/mkQuadro' int2str(i)],'commented','on');
        end

        % ACTIVATE THOSE NEEDED

        for i = 1:length(quads_number)
            set_param([modelname '/mkQuadro' int2str(quads_number(i))],'commented','off');
        end

        %% SIMULATION/EXPERIMENT

        if flag_simulationReal == 1 % SIMULATION
            for i = 1:length(quads_number)
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/velocity request'],'Host',['localhost:1000' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/velocity request'],'Instance',['maneuver_mk' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pom port'],'Host',['localhost:1000' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pom port'],'Instance',['pom_mk' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/mrsim port'],'commented','off');
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/mrsim port'],'Instance',['mrsim_mk' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/mrsim port'],'Host',['localhost:1000' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/state2pose_mk' int2str(quads_number(i))],'commented','off');
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pub_vrep_mk' int2str(quads_number(i))],'commented','off');
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pub_vrep_mk' int2str(quads_number(i))],'Topic',['/vrep/pos' int2str(quads_number(i))]);
            end
        else % REAL EXPERIMENT
            for i = 1:length(quads_number)
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/velocity request'],'Host',['jetson' int2str(quads_number(i)) ':1000' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/velocity request'],'Instance',['maneuver_mk' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pom port'],'Host',['jetson' int2str(quads_number(i)) ':1000' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pom port'],'Instance',['pom_mk' int2str(quads_number(i))]);
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/mrsim port'],'commented','on');
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/state2pose_mk' int2str(quads_number(i))],'commented','on');
                set_param([modelname '/mkQuadro' int2str(quads_number(i)) '/pub_vrep_mk' int2str(quads_number(i))],'commented','on');
            end
        end
    end
else
    modelname = 'apriltag_visual_servoing_2020b'; % simulink model to open.
    open(modelname);
end

disp('Simulink updated')
