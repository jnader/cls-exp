%%%%%%%%%%%%%%%%%%%%% INIT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defines all the parameters needed for the Simulations/Experiment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Be sure that the directory robots_interface and the sub directories are
% included in the MATLAB path
% -------------------------------------------------------------------------
% This file initialize the robots data structures with the relative
% parameters.
clear all

%% EXPERIMENT PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiment/Simulation flags/parameters

clsexp = 1;  % for pascal experiments (simulations or real exp)

flag_simulationReal = 2;    % 1: SIMULATION       2: REAL
joystickUse = 1;          % 1: Yes              0: NO

interactive_menu

% flag to log the data (1) or not (0)
log = 0;
log_name='simu_mk1';

% Define middleware
middleware='pocolibs';
% middleware='ros';

if flag_ViconQualisys == 1
    mocap_ip='192.168.30.1:801'; % IP of flylab-gs4 on WifiVicon network.
else
    mocap_ip='192.168.30.42'; %IP of flylab-gs3 on WifiVicon network.
end

% set parameters for groundstation
gs.pc = 'localhost';

% Defines a vector containing the quad_number relative to the robots that
% you want to use in the experiment (unless it's a demo, the quads_number
% is fixed)
if demo_choice == 1
    quads_number = [3];
end

%% Maneuver bound settings
% WARNING ! Size ofthe room/scene : if you go beyond boundarys, maneuver call such as land or takeoff will bring thequad back in 
% limitsMan = [-1.5, 1.5, -2.5, 2.5 0, 2.5, -12, 12]; % Valorisation room.
limitsMan = [-13.5, 13.5, -13.5, 13.5, 0, 10, -12, 12]; % Metivier room.

%% SETTINGS
gainJoy=5;
Tsim=0.002;

%% ROBOTS PARAMETERS
robots = cell(length(quads_number),1);
% For each robot define
%   quads{quad_number}.id = quad_numer
%   quads{quad_number}.pc = 'pc_name'
%   quads{quad_number}.mR = mass [kg]
%   quads{quad_number}.user='hostname';
%   quads{quad_number}.simP0=zeros(3,1); --> initial position simulation in
%   vrep world
for robot = 1:length(robots)
    if quads_number(robot) == 1
        quads{1}.id = 1;
        quads{1}.pc = 'jetson1';
        quads{1}.mR = 1.330; % battery = 0.340g (3300mAh).
        quads{1}.user='nvidia';
        quads{1}.simP0=[0;0;0.23;0];
        quads{1}.mocap_flag = flag_mocap;
        quads{1}.vo_flag = flag_vo;
        quads{1}.vs_flag = flag_vs;
        quads{1}.has_T265 = 0;
    elseif quads_number(robot) == 2
        quads{2}.id = 2;
        quads{2}.pc = 'jetson2';
        quads{2}.mR = 1.515; % battery = 0.340g (3300mAh).
        quads{2}.user='nvidia';
        quads{2}.simP0=[0;2;0.25;0];
        quads{2}.mocap_flag = flag_mocap;
        quads{2}.vo_flag = flag_vo;
        quads{2}.vs_flag = flag_vs;
        quads{2}.has_T265 = 1;
    elseif quads_number(robot) == 3
        quads{3}.id = 3;
        quads{3}.pc = 'jetson3';
        quads{3}.mR = 1.330; % battery = 0.340g (3300mAh).
        quads{3}.user='nvidia';
        quads{3}.simP0=[1;1;0.25;0];
        quads{3}.mocap_flag = flag_mocap;
        quads{3}.vo_flag = flag_vo;
        quads{3}.vs_flag = flag_vs;
        quads{3}.has_T265 = 0;
    elseif quads_number(robot) == 4
        quads{4}.id = 4;
        quads{4}.pc = 'jetson4';
        quads{4}.mR = 1.538; % battery = 0.340g (3300mAh).
        quads{4}.user='nvidia';
        quads{4}.simP0=[-1;-1;0.25;0];
        quads{4}.mocap_flag = flag_mocap;
        quads{4}.vo_flag = flag_vo;
        quads{4}.vs_flag = flag_vs;
        quads{4}.has_T265 = 0;
    elseif quads_number(robot) == 5
        quads{5}.id = 5;
        quads{5}.pc = 'jetson5';
        quads{5}.mR = 1.382;
        quads{5}.user='nvidia';
        quads{5}.simP0=[-1;-2;0.25;0];
        quads{5}.mocap_flag = flag_mocap;
        quads{5}.vo_flag = flag_vo;
        quads{5}.vs_flag = flag_vs;
        quads{5}.has_T265 = 0;
    elseif quads_number(robot) == 6
        quads{6}.id = 6;
        quads{6}.pc = 'jetson6';
        quads{6}.mR = 1.350;
        quads{6}.user='nvidia';
        quads{6}.simP0=[-2;-1;0.25;0];
        quads{6}.mocap_flag = flag_mocap;
        quads{6}.vo_flag = flag_vo;
        quads{6}.vs_flag = flag_vs;
        quads{6}.has_T265 = 0;
    end
end

init_global_settings

%% CLEAN WORKSPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -except flag_simulationReal flag_ViconQualisys robots quads_number c deadZoneJoystick gainJoy decreaseFactor middleware Tsim landHeight gs joystickUse ...
    K0 K1 K2 K3 log limitsMan experience_dir log_name drone_joystick_flag demo_choice clsexp

%% UPDATE SIMULINK MODEL WITH ACTUAL PARAMETERS
update_simulink

%% INITIALIZE GENOMIX COMPONENTS & RUN SIMULINK MODEL
init_genomix_robots

%% Configure and launch detection component if we choose drone detection demo
if demo_choice == 4
    launch_detection_component
end