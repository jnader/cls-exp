%% Land experiments

if clsexp==1
    reset_set_geom
end

for i=1:length(robots)
    current_state = robots{i}.pom.frame('robot');
    current_pos = current_state.frame.pos;
    current_att = current_state.frame.att;
    eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);
    yaw = eul_angles(1);
    landpos = [current_pos.x, current_pos.y, 0.3, 0];
    robots{i}.maneuver.set_velocity_limit(0.2,0.2)
    robots{i}.maneuver.goto('-a', landpos, 0);
end