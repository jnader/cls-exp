Short protocol to display pom logs from the terminal :

	1. $ cd /tmp
	
	2. find and copy the name of the file of interest (we refer to it as $FILENAME)
	
	3. $ gnuplot
	
	4. gnuplot> plot $FILENAME u 'x':'y' w l       -->    for xy graph
