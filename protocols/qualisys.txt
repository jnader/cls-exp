1. Ask for the Métivier room key at the reception, and also take the drone room key.


2. Open the Métivier room :

	- Switch the lights on
	- Power on the two power strips (one for the camera + flylab-gs3 and one for the laptop screen, mouse, keyboard, etc.)
	- Power on flylab-gs3 and connect to your session (AZERTY configuration keyboard but QWERTY display ! can be changed !) username = pbrault
	
3. Open "Qualisys Track Manager" (2021.2) and :

	- Open project 'C:\Users\Public\Qualisys\Métivier'
	- Then 'File'->'New' or CTRL+N to se the cameras display
	- Then 'Tools'->'Project Options'->'Load Bodies' and select 'mkQuadro1.xml' if using Jetson1
	- '3D' on the left tools bar to see the 3D scene: if jetson1 is inside, it should be clearly visible
