% This script is used after `init.m` script, when 

if flag_simulationReal == 1
    rosinit;
    chatpub = rospublisher('/vrep/pos1', 'geometry_msgs/Pose');
    msg = rosmessage(chatpub);
end

%% LOAD TRAJECTORY :

load trajs_and_per/traj0_jetson.mat
load trajs_and_per/30_perturbations.mat

traj_num = 4; % [5 to 29] [0 to 0 done] select which traj you want to experiment
opt_cond = 'PI_opt'; % 'IG_InputSat' or 'IG_Target' or 'PI_opt' (or 'dummy')
pcase = 1;  % 0: nominal, 1: perturbed

ts = 0.05;  % timestep for Simulink simulation
waitbeforestart = 1; % wait 1s of simulation before start (cause pc slow)
endpause = 1;  % rest time after trajectory, before moving to origin
gohomecoeff = 3;  % go home after traj duration coefficient from output diff norm
zoffbeforetakeooff = 0;
display_traj = 0;

p_nom = [6.5e-4, 0.0154, 0, 0]; % kf, ktau, gx, gy
dev = 0.1;  % 10 percent 
off = 0.03;  % 3 cm


if strcmp(opt_cond, 'dummy') == 1
    wp = [0, 0, 0, 0,   1.5, 0.5, 0, 0,  3, 1, 0, 0;
          0, 0, 0, 0,   0.5, 0.2, 0, 0,  0, 0, 0, 0;
          0, 0, 0, 0,     0,   0, 0, 0,  0, 0, 0, 0];
    wp_t = [0, 2.5, 5];
    wpm = wp;
    for idx=1:4:size(wpm, 2)-4+1
        wpm(:, idx) = - wpm(:, idx);  % opposite speed and acc for x axis
    end
    wp = [wp, wpm];
    icut = floor(size(wp, 2)/2);
else
    % Get trajectory (waypoints and time of waypoints):
    wp = [];
    wp_t = eval(['traj_' num2str(traj_num) '.' num2str(opt_cond) '_wp_t']);
    for i = 1:length(wp_t)
        wp_tmp = eval(['traj_' num2str(traj_num) '.' num2str(opt_cond) '_wp']);
        wp = [wp squeeze(wp_tmp(i, :, :))];
    end
    % Generate mirror traj on x axis
    wpm = wp;
    for idx=1:4:size(wpm, 2)-4+1
        wpm(:, idx) = - wpm(:, idx);  % opposite speed and acc for x axis
    end
    wp = [wp, wpm];
    icut = floor(size(wp, 2)/2);
end

t_traj = linspace(0, wp_t(end), floor(100*wp_t(end)));
xyzyaw_t = zeros(4, length(t_traj));

for i = 1:length(t_traj)
    xyzyaw_t = [xyzyaw_t waypoints2trajectory(wp_t, wp(:, 1:icut), t_traj(i))'];
end

if display_traj == 1
    figure;
    set(gcf,'Units','centimeters','Position',[1 2 16 9],'Color', 'w');
    
    plot3(xyzyaw_t(1, :), xyzyaw_t(2, :), xyzyaw_t(3, :), 'LineWidth', 2);
    grid on
    axis equal
end
 

if pcase == 1
    if strcmp(opt_cond, 'dummy') == 1
        max_traj = 6;
    else
        max_traj = 18; % should be a multiple of rep (below)
    end
    rep = 3;
    n_per = max_traj/rep;
    flight_num = input('Select perturbed flight number (1-5)...');
    idxe = flight_num*n_per - 1;
    idxs = idxe - n_per + 1;
    p_exp = [];
    for i=idxs:idxe
        p_exp = [p_exp; repmat(eval(['per_' num2str(i)]), rep, 1)];
    end
    disp(p_exp)
else
    if strcmp(opt_cond, 'dummy') == 1
        max_traj = 4;
    else
        max_traj = 10;
    end
    p_exp = repmat(p_nom, max_traj, 1);
    disp(p_exp)
end

Dxyzyaw = max(xyzyaw_t, [], 2) - min(xyzyaw_t, [], 2);
Dx = Dxyzyaw(1);
fprintf('\nFor this trajectory : \n\nDx = %f [m],\nDy = %f [m],\nDz = %f [m],\nDyaw = %f [rad].\n\n', Dxyzyaw(1), Dxyzyaw(2), Dxyzyaw(3), Dxyzyaw(4));

Dzmax = 1.5; % maximum altitude variation in m
zmargin = Dzmax - Dxyzyaw(3);
if strcmp(opt_cond, 'dummy') == 1
    takeoffalt = 0.4 + zoffbeforetakeooff;
else
    takeoffalt = 0.6 - min(xyzyaw_t(3, :)) + zmargin/2 - zoffbeforetakeooff;
end
    
if zmargin > 0.1
    fprintf('\nDrone will take off at %f [m] \n\n', takeoffalt);
else
    fprintf('\nz margin is not sufficient...\n\n');
end

%%%

reset_set_geom

start_robots(robots,gs,log,flag_simulationReal, middleware);
mikroServo_robots(robots);
set_current_position_robots(robots);
set_current_state_robots(robots);
nhfcServo_robots(robots);
% reset_nhfc;
if flag_simulationReal == 1
    forceSimPosition(robots)
end

if flag_simulationReal==1
    pause(3) % pause before takeoff to prevent simulation fails
    robots{1}.maneuver.take_off('-a', takeoffalt, 0); % Take off
    
    for i = 1:100  % update coppeliasim for takeoff
        % Read current drone's state.
        current_state = robots{1}.pom.frame('robot');
        current_pos = current_state.frame.pos;
        current_att = current_state.frame.att;
        eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);

        % Publish state on `/vrep/pos1` topic.
        msg.Position.X = current_state.frame.pos.x;
        msg.Position.Y = current_state.frame.pos.y;
        msg.Position.Z = current_state.frame.pos.z;

        msg.Orientation.X = current_state.frame.att.qx;
        msg.Orientation.Y = current_state.frame.att.qy;
        msg.Orientation.Z = current_state.frame.att.qz;
        msg.Orientation.W = current_state.frame.att.qw;

        send(chatpub, msg);

        pause(0.05);
    end
else
    input('Takeoff now ?...')  % wait for propellers to turn fast enough
    robots{1}.maneuver.take_off('-a', takeoffalt, 0); % Take off
    pause(1)
end

current_state = robots{1}.pom.frame('robot');
current_pos = current_state.frame.pos;
current_att = current_state.frame.att;
eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);
yaw = eul_angles(1);
%wp(1, 1:icut) = wp(1, 1:icut) + repmat(current_position, 1, length(wp_t));

% Saving origin to use it in `return_to_home` function.
%x0 = wp(1,1);
%y0 = wp(1,2);
%z0 = wp(1,3);
%yaw0 = wp(1,4);
x0 = current_pos.x;
x0r = x0 + Dx;
y0 = current_pos.y;
if strcmp(opt_cond, 'dummy') == 1
    z0 = takeoffalt - zoffbeforetakeooff; % add little movement before 1st run
else
    z0 = takeoffalt + zoffbeforetakeooff; % add little movement before 1st run
end
yaw0 = yaw;

dataforexp = [x0, x0r, y0, z0, yaw0, ts, endpause, gohomecoeff, waitbeforestart];  % to use in the Simulink

if flag_simulationReal == 1
    rosshutdown;
end

sim('cls_trajectory_20b');

run monitor.m

if pcase == 0
    par_fold = 'nom';
else
    par_fold = ['per_f' num2str(flight_num)];
end

if flag_simulationReal == 2
    scplogs = ['scp nvidia@jetson' num2str(robots{1}.id) ':/tmp/*.log /home/scalp/Documents/github/cls-savelogs/traj_' ...
        num2str(traj_num) '/' opt_cond '/' par_fold];
    system(scplogs);
end
