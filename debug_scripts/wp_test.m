left_origin = [-1.8, 0.3, 0.4, 0.1];
current_pos = [-1.7, 0.3, 0.3, -0.2];

ts = 0.05;

N_jc = 3;
N_dim = 4;

vmax = 0.5;
amax = 1;

p1 = [0, 0, 0, 0];
%p1 = [1.9, 0.3, 0.2, 0];
p2 = [1, 0, 0, 0];

vec = p2 - p1;

[maxdiff, idx] = max(abs(p2-p1)); % get max with index

disp(idx)

if (maxdiff > vmax^2/amax)  % velocity trapezium
    dt1 = vmax/6*amax; % 0 to vmax
    dt2 = maxdiff/vmax - vmax/2*amax; % at vmax
    dt3 = dt1; % vmax to 0
    wp_t = [0, dt1, dt1 + dt2, dt1 + dt2 + dt3]; % wp_t is computed
    wp1 = zeros(N_jc, N_dim);
    wp2 = wp1; wp3 = wp1; wp4 = wp1;
    wp1(1, :) = p1; wp4(1, :) = p2;  % start and end
    for i = 1:N_dim
        if i == idx % dimensioning axis
            wp2(1, idx) = p1(idx) + sign(vec(idx))*amax*dt1^2;
            wp3(1, idx) = p2(idx) - sign(vec(idx))*amax*dt3^2;
            wp2(2, idx) = sign(vec(idx))*vmax;
            wp3(2, idx) = sign(vec(idx))*vmax;
        else
            a = vec(i)/(dt1*(dt1 + dt2));
            v = a*dt1;
            wp2(1, i) = p1(i) + a*dt1^2;
            wp3(1, i) = p2(i) - a*dt3^2;
            wp2(2, i) = v; 
            wp3(2, i) = v;
        end
    end
    wp = [wp1, wp2, wp3, wp4];
else                        % velocity triangle
    dt1 = sqrt(maxdiff/amax);
    dt2 = dt1;
    wp_t = [0, dt1, dt1 + dt2];
    wp1 = zeros(N_jc, N_dim); wp2 = wp1; wp3 = wp1;
    wp1(1, :) = p1; wp3(1, :) = p2;  % start and end
    wp2(1, :) = (p1 + p2)/2; % middle
    for i=1:N_dim
        if i == idx % dimensioning axis
            wp2(2, idx) = sign(vec(idx))*amax*dt1;
        else
            wp2(2, i) = vec(i)/dt1;
        end
    end
    wp = [wp1, wp2, wp3];
end

wp1 = zeros(3, 4); wp2 = zeros(3,4);
wp1(1, :) = p1; wp2(1, :) = p2;
wp = [wp1, wp2];
wp_t = [0, 4*norm(vec)];

t = linspace(wp_t(1), wp_t(end), floor((wp_t(end) - wp_t(1))/ts));

posref = zeros(length(t), N_dim);
for i = 1:length(t)
    posref(i, :) = waypoints2trajectory(wp_t, wp, t(i));
end

velref = diff(posref)/ts;
accref = diff(velref)/ts;

%% PLOT


% pos

figure(1);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelpos = {'x [m]', 'y [m]', 'z [m]', ' \psi [rad]'};

for i=1:4
    subplot(2, 2, i)
    plot(t, posref(:, i), 'LineWidth', 2, 'Color', 'b')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelpos{i}, 'FontSize', 14)
    title('Position', 'FontSize', 16)
    grid on
end

% vel

figure(2);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelvel = {'v_x [m.s^{-1}]', 'v_y [m.s^{-1}]', 'v_z [m.s^{-1}]', 'v_{\psi} [rad.s^{-1}]'};

for i=1:4
    subplot(2, 2, i)
    plot(t(1:end-1), velref(:, i), 'LineWidth', 2, 'Color', 'k')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelvel{i}, 'FontSize', 14)
    title('Velocity', 'FontSize', 16)
    grid on
end


% acc

figure(3);
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

labelacc = {'a_x [m.s^{-2}]', 'a_y [m.s^{-2}]', 'a_z [m.s^{-2}]', 'a_{\psi} [rad.s^{-2}]'};

for i=1:4
    subplot(2, 2, i)
    plot(t(1: end-2), accref(:, i), 'LineWidth', 2, 'Color', 'r')
    xlabel('t [s]', 'FontSize', 14)
    ylabel(labelacc{i}, 'FontSize', 14)
    grid on
end

   