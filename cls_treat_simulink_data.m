%% Display ellipses for one trajectory and save data

close all

pathlogs = '/home/scalp/Documents/github/cls-savelogs/';

load trajs_and_per/30trajsforexp.mat

traj_num = 0; % linspace(0, 29, 30) for 30 trajectories
opt_cond = {'IG_InputSat', 'IG_Target', 'PI_opt'};
target = eval(['traj_' num2str(traj_num) '.TARGET']);
target = target(1, :);

%% LOAD .mat DATA:

pathtraj = [pathlogs 'traj_' num2str(traj_num)];

res = zeros(63, 4); % 3 nom, 60 perturbations

countnom = 1; % start at 1 to fill 1, 2, 3 for nom flights
countper = 4;  % start at 4 to fill 4 to 63 for per flights
nomclouds = {};

for j=1:length(opt_cond)
    if j == 1
        pcase = {'nom'};  % IG_InputSat, only nom, no perturbed flights
    else
        if (traj_num == 1) || (traj_num == 2)
            pcase = {'nom', 'per_f1', 'per_f2', 'per_f3', 'per_f4', 'per_f5', 'per_f6'};
        else
            pcase = {'nom', 'per_f1', 'per_f2', 'per_f3', 'per_f4', 'per_f5'};
        end
    end
        for k=1:length(pcase)
            pathflight = [pathtraj '/' opt_cond{j} '/' pcase{k} '/'];
            matfile = dir(fullfile(pathflight, '*.mat')); % get matfile name from folder
            load([pathflight matfile.name], 'tref', 'expstate', 'posref', 'posquat'); % load flight .mat file and import variables
            posref = posref';
            startindices = find(tref == 0 & (expstate == 4.5 | expstate == 2.5));
            endindices = find(tref == 5 & (expstate == 1 | expstate == 3));
            nbruns = length(startindices);
            finaloutputs = ones(nbruns, 4);
            for i=1:nbruns
                outputoffset = posref(startindices(i), :);
                finalposwithoffset = posquat(endindices(i), 1:3);
                finalquatwithoffset = posquat(endindices(i), 4:7); 
                finaleulwithoffset = quat2eul(finalquatwithoffset);
                finalyawwithoffset = finaleulwithoffset(1);
                finaloutput = [finalposwithoffset, finalyawwithoffset] - outputoffset;
                if mod(i, 2) == 0
                    finaloutput(1) = - finaloutput(1);
                end
                finaloutputs(i, :) = finaloutput;
            end
            
            if (nbruns == 15) || (nbruns == 18)
                rep = 3;
                for i=1:nbruns/3
                    idxs = 1 + 3*(i-1);
                    idxe = 3*i;
                    res(countper, :) = mean(finaloutputs(idxs:idxe, :));
                    countper = countper + 1;
                end   
            elseif nbruns == 10
                nomclouds{j} = finaloutputs;
                res(countnom, :) = mean(finaloutputs);
                countnom = countnom + 1;
            else
                disp('Number of runs incorrect, check data')
            end
        end
end

trajdata.IG_InputSat.nom = res(1, :);
trajdata.IG_Target.nom = res(2, :);
trajdata.PI_opt.nom = res(3, :);
trajdata.IG_Target.per = res(4:33, :);
trajdata.PI_opt.per = res(34:63, :);

%% PLOT DATA/ELLIPSES

% nominal ellipses

targetnomxy = nomclouds{2}(:, 1:2);
targetnomzyaw = nomclouds{2}(:, 3:4);
pioptnomxy = nomclouds{3}(:, 1:2);
pioptnomzyaw = nomclouds{3}(:, 3:4);

targetnomxyell = get_ellipse(targetnomxy, 2);
targetnomzyawell = get_ellipse(targetnomzyaw, 2);
pioptnomxyell = get_ellipse(pioptnomxy, 2);
pioptnomzyawell = get_ellipse(pioptnomzyaw, 2);

% perturbed

targetperxy = trajdata.IG_Target.per(:, 1:2);
targetperzyaw = trajdata.IG_Target.per(:, 3:4);
pioptperxy = trajdata.PI_opt.per(:, 1:2);
pioptperzyaw = trajdata.PI_opt.per(:, 3:4);
targetperxyell = get_ellipse(targetperxy, 2);
targetperzyawell = get_ellipse(targetperzyaw, 2);
pioptperxyell = get_ellipse(pioptperxy, 2);
pioptperzyawell = get_ellipse(pioptperzyaw, 2);

figure;
set(gcf,'Units','centimeters','Position',[1 2 20 9], 'Color', 'w');

subplot(1, 2, 1)
%scatter(trajdata.IG_InputSat.nom(1), trajdata.IG_InputSat.nom(2), 'y')
scatter(trajdata.IG_Target.nom(1), trajdata.IG_Target.nom(2), 'r')
hold on
scatter(trajdata.PI_opt.nom(1), trajdata.PI_opt.nom(2), 'g')
%scatter(trajdata.IG_Target.per(:, 1), trajdata.IG_Target.per(:, 2), 'kx')
%scatter(trajdata.PI_opt.per(1:end-3, 1), trajdata.PI_opt.per(1:end-3, 2), 'bx')
plot(targetperxyell(1, :), targetperxyell(2, :), 'k')
plot(pioptperxyell(1, :), pioptperxyell(2, :), 'b')
plot(targetnomxyell(1, :), targetnomxyell(2, :), 'r')
scatter(targetnomxy(1, :), targetnomxy(2, :), 'rx')
plot(pioptnomxyell(1, :), pioptnomxyell(2, :), 'g')
scatter(pioptnomxy(1, :), pioptnomxy(2, :), 'gx')

legend('IG-target_{nom}', 'PI-opt_{nom}', 'IG-target_{per}', 'PI-opt_{per}')
xlabel('x [m]');
ylabel('y [m]');
grid on

subplot(1, 2, 2)
%scatter(trajdata.IG_InputSat.nom(3), trajdata.IG_InputSat.nom(4), 'y')
scatter(trajdata.IG_Target.nom(3), trajdata.IG_Target.nom(4), 'r')
hold on
scatter(trajdata.PI_opt.nom(3), trajdata.PI_opt.nom(4), 'g')
%scatter(trajdata.IG_Target.per(:, 3), trajdata.IG_Target.per(:, 4), 'kx')
%scatter(trajdata.PI_opt.per(1:end-3, 3), trajdata.PI_opt.per(1:end-3, 4), 'bx')
plot(targetperzyawell(1, :), targetperzyawell(2, :), 'k')
plot(pioptperzyawell(1, :), pioptperzyawell(2, :), 'b')
plot(targetnomzyawell(1, :), targetnomzyawell(2, :), 'r')
scatter(targetnomzyaw(1, :), targetnomzyaw(2, :), 'rx')
plot(pioptnomzyawell(1, :), pioptnomzyawell(2, :), 'g')
scatter(pioptnomzyaw(1, :), pioptnomzyaw(2, :), 'gx')

legend('IG-target_{nom}', 'PI-opt_{nom}', 'IG-target_{per}', 'PI-opt_{per}')
xlabel('z [m]');
ylabel('\psi [m]');
grid on

function outputellipse = get_ellipse(data, std)

    for k=1:2

        %# substract mean
        Mu = mean(data);
        X0 = bsxfun(@minus, data, Mu);
        
        conf = 2*normcdf(std)-1;     %# covers around 95% of population
        scale = chi2inv(conf,2);     %# inverse chi-squared with dof=#dimensions

        Cov = cov(X0) * scale;
        [V, D] = eig(Cov);
        [D, order] = sort(diag(D), 'descend');
        D = diag(D);
        V = V(:, order);

        t = linspace(0,2*pi,100);
        e = [cos(t) ; sin(t)];        %# unit circle
        VV = V*sqrt(D);               %# scale eigenvectors
        outputellipse = bsxfun(@plus, VV*e, Mu'); %#' project circle back to orig space

        %# plot cov and major/minor axes
        %#quiver(Mu(1),Mu(2), VV(1,1),VV(2,1), 'Color','k')
        %#quiver(Mu(1),Mu(2), VV(1,2),VV(2,2), 'Color','k')
    end
end

