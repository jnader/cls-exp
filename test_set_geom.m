kf = p_exp(1, 1);
ktau = p_exp(1, 2);
gx = p_exp(1, 3);
gy = p_exp(1, 4);

d = 0.23;

G = [0.,    0.,   0.,    0.,   0., 0., 0., 0., ...
0.,    0.,   0.,    0.,   0., 0., 0., 0., ...
kf,    kf,   kf,    kf,   0., 0., 0., 0., ...
-gy * kf,  (-gy+d) * kf,   -gy * kf, -(gy+d) * kf,   0., 0., 0., 0., ...
-(-gx + d) * kf,    gx * kf, (gx + d) * kf,   gx * kf,   0., 0., 0., 0., ...
ktau*kf, -ktau*kf, ktau*kf, -ktau*kf,   0., 0., 0., 0.];

J = [0.015,    0.,    0., ...
      0.,    0.015,    0., ...
      0.,       0., 0.015];

for i=1:length(robots)
    if flag_simulationReal == 1
        robots{i}.nhfc.set_geom(robots{i}.param.mR, num2cell(G), num2cell(J));
    else
        if robots{i}.id == 1
            robots{i}.nhfc.set_geom(num2cell(G), num2cell(J)); % old version 
        else
            robots{i}.nhfc.set_geom(robots{i}.param.mR, num2cell(G), num2cell(J)) % new version
        end
    end
end

p_exp = p_exp(2:end, :);

